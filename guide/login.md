---
label: Login
order: 100
---

# Login

Mangadex allows you to personalize your feed, keep track of your reading activity, make lists of your favorite manga, and more. To login, you first need to have an account, which can only be made through the [website](https://mangadex.org/account/signup) as the endpoint is protected by ReCaptcha.

## Making a session

```http
 POST /auth/login
```

Logins are handled by serving a JWT session token along with an expiry date, and a refresh token with which we can use to make a new session without logging in again.

!!!info
The session token expires after 15 minutes, and the refresh token after 1 month.
!!!


##### Request

Supposing an account with username "testuser" and "testpass" exists, we proceed like this.

+++Python

:::code-block
```python
creds = {
    'username': 'testuser',
    'password': 'testpass'
}
```
:::

:::code-block
```python
import requests
from datetime import datetime

baseUrl = 'https://api.mangadex.org'

r = requests.post(
    f'{baseUrl}/auth/login',
    json=creds
)

sessionToken = r.json()['token']['session']
expires = datetime.now().timestamp() + 15 * 60000
refreshToken = r.json()['token']['refresh']

print(sessionToken, expires, refreshToken)
```
:::

+++JavaScript

:::code-block
```javascript
const creds = {
    username: 'testuser',
    password: 'testpass'
};
```
:::

:::code-block
```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

let sessionToken, expires, refreshToken;

(async () => {
    const resp = await axios({
        method: 'POST',
        url: `${baseUrl}/auth/login`,
        headers: {
            'Content-Type': 'application/json'
        },
        data: creds
    });

    sessionToken = resp.data.token.session;
    expires = new Date().valueOf() + 15 * 60000
    refreshToken = resp.data.token.refresh;

    console.log(sessionToken, expires, refreshToken);
})();

```
:::

+++

## Refreshing a session

```http
 POST /auth/refresh
```

Once a session expires, you have to call this endpoint to acquire a new one.


##### Request

+++Python

:::code-block
```python
refreshToken = 'yourrefreshtokenhere'
```
:::

:::code-block
```python
import requests
from datetime import datetime

baseUrl = 'https://api.mangadex.org'

r = requests.post(
    f'{baseUrl}/auth/refresh',
    json={
        'token': refreshToken
    }
)

sessionToken = r.json()['token']['session']
expires = datetime.now().timestamp() + 15 * 60000

print(sessionToken, expires, refreshToken)
```
:::

+++JavaScript

:::code-block
```javascript
const refreshToken = 'yourrefreshtokenhere';
```
:::

:::code-block
```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

(async () => {
    const resp = await axios({
        method: 'POST',
        url: `${baseUrl}/auth/refresh`,
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            token: refreshToken
        }
    });

    const sessionToken = resp.data.token.session;
    const expires = new Date().valueOf() + 15 * 60000

    console.log(sessioonToken, expires, refreshToken)
})();

```
:::

+++
